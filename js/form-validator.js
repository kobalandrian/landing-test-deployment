document.addEventListener("DOMContentLoaded", function (event) {   
    
    // Подставляем код телефона
    function addMaskPhone() {
      var mask = {
        getCountrySelects: function () {
          var selects = document.querySelectorAll('.country_select');
          return (selects.length) ? selects : 0;
        },

        getPhones: function () {
          var phones = document.querySelectorAll('.wv_phone');
          return (phones.length) ? phones : 0;
        },

        init: function () {
          var selects = mask.getCountrySelects();
          var phones = mask.getPhones();
          if (selects && phones) {

            //выставляем дефолтный код
            var countryCode = selects[ 0 ].value.toLowerCase();
            var countryCodes = {
              'tj': '+992',
            };

            selects.forEach(function (select) {
              select.addEventListener('change', function () {
                countryCode = this.value;
                selects.forEach(function (sel) {
                  sel.value = countryCode;
                });
              });
            });

            phones.forEach(function (phone) {
              phone.pattern = '(\\+)[0-9]{12}';
              phone.title = 'Телефон должен содержать только 12 цифр';

              //при попадании фокуса оставляем код + введенную часть номера
              phone.addEventListener('focusin', function () {
                var code = countryCodes[ countryCode.toLowerCase() ];cd
                this.value = !(this.value.length > code.length) ? code : this.value;
              });

              //при вводе блокируем удаление кода страны
              phone.addEventListener('input', function () {
                var code = countryCodes[ countryCode.toLowerCase() ];
                this.value.indexOf(code) && (this.value = code);
              });
            });
          }
        },
      };

      mask.init();

    }
  addMaskPhone();
});
