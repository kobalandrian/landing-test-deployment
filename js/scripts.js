"use strict";

(function (factory) {
  typeof define === 'function' && define.amd ? define('scripts', factory) : factory();
})(function () {
  'use strict';

  var disableConfirmation = false;
  window.addEventListener("beforeunload", function (event) {
    if (!disableConfirmation) {
      event.preventDefault();
      var formModal = document.querySelector('[data-modal="comeback"]');
      console.dir(formModal);
      openPopup$1(formModal);
      return event.returnValue = "Are you sure you want to exit?";
    } else {
      disableConfirmation = false;
    }
  }); //need to close beforeunload event at the time of the click and submit event 

  document.addEventListener('click', function (event) {
    if (event.target.tagName.toLowerCase() === 'a') {
      disableConfirmation = true;
    }
  });
  document.addEventListener('submit', function (event) {
    disableConfirmation = true;
  }); // end logic

  function openPopup$1(modal) {
    modal.classList.add("active");
    closePopup$1(modal);
  }

  function closePopup$1(modal) {
    var modalCloseBtn = modal.querySelectorAll("[data-modal-close]");
    modalCloseBtn.forEach(function (btn) {
      return btn.addEventListener("click", function (evt) {
        modal.classList.remove("active");
      });
    });
  }

  var date = document.querySelectorAll('[data-date]'),
      timer = document.querySelectorAll('[data-timer]');
  if (date) showDate(date);
  if (timer) runTimer(timer, [1, 3, 45]);
  var currentYear = document.querySelector('[data-current-year]');
  if (currentYear) currentYear.textContent = new Date().getFullYear();

  function showDate(date) {
    var today = new Date();
    date.forEach(function (item) {
      return function () {
        var tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        var day = tomorrow.getDate();
        day = day < 10 ? '0' + day : day;
        var month = tomorrow.getMonth();
        month = month < 10 ? '0' + month : month;
        var year = tomorrow.getFullYear();
        var pattern = item.dataset.date;
        item.textContent = pattern.replace("day", day).replace("month", month).replace("year", year);
      }();
    });
  }

  function runTimer(obj, startTime) {
    var timer = {
      day: startTime[0],
      hour: startTime[1],
      min: startTime[2]
    };
    setTimer(obj, timer);
    var timerInterval = setInterval(function () {
      if (timer.min > 0) {
        timer.min--;
      } else if (timer.hour > 0) {
        timer.hour--;
        timer.min = 59;
      } else if (timer.day > 0) {
        timer.day--;
        timer.hour = 11;
        timer.min = 59;
      } else {
        clearInterval(timerInterval);
      }

      setTimer(obj, timer);
    }, 1000 * 60);
  }

  function setTimer(obj, timer) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = obj[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var item = _step.value;
        var time = item.dataset.timer != "day" && timer[item.dataset.timer] < 9 ? " 0".concat(timer[item.dataset.timer], " ") : " ".concat(timer[item.dataset.timer], " ");
        item.textContent = time;
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator["return"] != null) {
          _iterator["return"]();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }

  var newActiveLink;
  var menu = document.querySelector("[data-menu]"),
      links$1 = menu.querySelectorAll("[data-link]"),
      linkedBlock = document.querySelectorAll("[data-linked-block]");

  var mainHeaderToggle = function mainHeaderToggle() {
    if (!menu) return;
    var hamburger = menu.querySelector("[data-menu-open]");

    if (hamburger) {
      hamburger.addEventListener('click', function () {
        menu.classList.toggle('active');
        hamburger.classList.toggle('active');
      });
    }

    if (links$1) {
      links$1.forEach(function (link) {
        return link.addEventListener('click', function () {
          menu.classList.remove('active');
          hamburger.classList.remove('active');
          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = links$1[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var _link = _step2.value;
              if (_link.classList.contains('active')) _link.classList.remove('active');
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
                _iterator2["return"]();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          this.classList.add('active');
        });
      });
    }
  };

  var doLinkActive = function doLinkActive() {
    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
      for (var _iterator3 = linkedBlock[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var block = _step3.value;

        if (block.getBoundingClientRect().height + block.getBoundingClientRect().top > 200) {
          newActiveLink = menu.querySelector("[href=\"#".concat(block.dataset.linkedBlock, "\"]"));

          if (newActiveLink) {
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
              for (var _iterator4 = links$1[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                var link = _step4.value;
                link.classList.remove('active');
              }
            } catch (err) {
              _didIteratorError4 = true;
              _iteratorError4 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion4 && _iterator4["return"] != null) {
                  _iterator4["return"]();
                }
              } finally {
                if (_didIteratorError4) {
                  throw _iteratorError4;
                }
              }
            }

            newActiveLink.classList.add('active');
          }

          break;
        }
      }
    } catch (err) {
      _didIteratorError3 = true;
      _iteratorError3 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
          _iterator3["return"]();
        }
      } finally {
        if (_didIteratorError3) {
          throw _iteratorError3;
        }
      }
    }
  };

  mainHeaderToggle();

  window.onscroll = function () {
    // mainHeaderFixed();
    if (linkedBlock && links$1) doLinkActive();
  };

  var links = document.querySelectorAll("[data-link]");
  var _iteratorNormalCompletion5 = true;
  var _didIteratorError5 = false;
  var _iteratorError5 = undefined;

  try {
    for (var _iterator5 = links[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
      var link = _step5.value;
      link.addEventListener('click', goToBlock);
    }
  } catch (err) {
    _didIteratorError5 = true;
    _iteratorError5 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion5 && _iterator5["return"] != null) {
        _iterator5["return"]();
      }
    } finally {
      if (_didIteratorError5) {
        throw _iteratorError5;
      }
    }
  }

  function goToBlock(e) {
    e.preventDefault();
    var block = document.getElementById(this.dataset.link);
    block.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }

  var modalOpenBtn = document.querySelectorAll('[data-modal-open]'),
      comebackModal = document.querySelector('[data-modal="comeback"]'),
      notificationModal = document.querySelector('[data-modal="notification"]'),
      notificationOrderModal = document.querySelector('[data-modal="notification-order"]');
  var modal;
  if (notificationModal) openPopup(notificationModal);
  if (comebackModal) setTimeout(openPopup, 8000, comebackModal);
  if (notificationOrderModal) setTimeout(openPopup, 15000, notificationOrderModal);
  modalOpenBtn.forEach(function (btnItem) {
    return btnItem.addEventListener("click", openPopupModal);
  });

  function openPopupModal(evt) {
    evt.preventDefault();
    var dataPopup = this.dataset.modalOpen;
    modal = document.querySelector("[data-modal=\"".concat(dataPopup, "\"]"));
    openPopup(modal);
  }

  function closePopup(modal) {
    var modalCloseBtn = modal.querySelectorAll("[data-modal-close]");
    modalCloseBtn.forEach(function (btn) {
      return btn.addEventListener("click", function (evt) {
        modal.classList.remove("active");
      });
    });
  }

  function openPopup(modal) {
    modal.classList.add("active");
    closePopup(modal);
  }

  window.addEventListener("keydown", function (evt) {
    if (evt.keyCode === 27) {
      if (modal.classList.contains("active")) {
        evt.preventDefault();
        modal.classList.remove("active");
      }
    }
  });
  var galleries = document.querySelectorAll("[data-gallery]");

  if (galleries.length != 0) {
    galleries.forEach(function (gallery) {
      new Gallery(gallery).setup();
    });
  }

  function Gallery(gallery) {
    this.gallery = gallery;
    this.type = gallery.dataset.gallery;
    this.btns = gallery.querySelectorAll("[data-gallery-click]");
    this.slideItem = 0;

    this.setup = function () {
      var _this = this;

      var _iteratorNormalCompletion6 = true;
      var _didIteratorError6 = false;
      var _iteratorError6 = undefined;

      try {
        for (var _iterator6 = this.btns[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
          var btn = _step6.value;
          btn.addEventListener("click", function (e) {
            e.preventDefault();
            _this.slideItem = _this.slideItem + +e.currentTarget.dataset.galleryClick;

            _this._slide();
          });
        }
      } catch (err) {
        _didIteratorError6 = true;
        _iteratorError6 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion6 && _iterator6["return"] != null) {
            _iterator6["return"]();
          }
        } finally {
          if (_didIteratorError6) {
            throw _iteratorError6;
          }
        }
      }

      this._slide();
    };

    this._slide = function () {
      if (this.type == 'show') {
        this._showSlider();
      }

      if (this.type == 'scroll') {
        this._scrollSlider();
      } else {
        this._changeSlide();
      }
    };

    this._showSlider = function () {
      var _this2 = this;

      var items = this.gallery.querySelectorAll("[data-gallery-item]");
      if (this.slideItem >= items.length) this.slideItem = 0;
      if (this.slideItem < 0) this.slideItem = items.length - 1;
      items.forEach(function (item, key) {
        if (key != _this2.slideItem) {
          item.classList.add('hide');
        } else {
          item.classList.remove('hide');
        }
      });
    };

    this._scrollSlider = function () {};

    this._changeSlide = function () {};
  }

  var boxes = gsap.utils.toArray('[animate]');
  boxes.forEach(function (box, i) {
    ScrollTrigger.create({
      trigger: box,
      start: 'top bottom-=100px',
      end: 'bottom top',
      toggleActions: 'play complete none none',
      onEnter: function onEnter() {
        box.classList.add('is-active');
      },
      once: false // markers: true,

    });
  });
});