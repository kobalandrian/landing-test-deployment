<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

// Retrieve form data
$name = $_POST['name'];
$phone = $_POST['phone'];
$url = $_POST['url'];

// Create a new PHPMailer instance
$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';
$mail->setLanguage('ru', 'phpmailer/language/');
$mail->isHTML(true);

try {
    //Recipients
    $mail->setFrom('a.kobal@webvork.com', 'Test');
    $mail->addAddress('kobalandrian@gmail.com');     //Add a recipient

    // Content
    $mail->Subject = 'Order from website' . $url;
    $mail->Body = 'New order from website' . $url . '. Client name:' . $name . '. Client phone:' . $phone . '.';

    // Send email
    $mail->send();
    header("Location: success.html");
} catch (Exception $e) {
    echo 'Message could not be sent. Error: ', $mail->ErrorInfo;
}
